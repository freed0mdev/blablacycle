require('rootpath')();
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var rides = require('./routes/rides');
var cities = require('./routes/cities');
var users = require('./routes/users');

var port = 5000;

var app = express();

var cors = require('cors');
var expressJwt = require('express-jwt');
var config = require('./../config.json');

app.use(cors());

//View Engine
app.set('views', path.join(__dirname, '../dist'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Set Static Folder
app.use(express.static(path.join(__dirname, '../dist')));

//Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.use('/users', require('./controllers/user.controller'));
app.use('/api', rides);
app.use('/api', cities);
app.use('/api', users);
app.use('*', index);

// use JWT auth to secure the api
app.use(expressJwt({ secret: config.secret }).unless({ path: ['/users/authenticate', '/users/register'] }));

app.listen(port, function () {
    console.log('Server started on port ' + port);
});