var express = require('express');
var router = express.Router();

///MongoDB
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var MongoClient = require('mongodb').MongoClient;
var URL = "mongodb://localhost:27017/umtb";

const COLLECTION = 'rides';


// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}

// GET: finds all rides
router.get('/' + COLLECTION, function (req, res, next) {
    MongoClient.connect(URL, function(err, db) {
        if (err) {
            res.send(err);
        }
        var mysort = { date: 1 };
        db.collection(COLLECTION).find({}).sort(mysort).toArray(function(err, rides) {
            if (err) {
                handleError(res, err.message, "Failed to get rides.");
            } else {
                res.status(200).json(rides);
                db.close();
            }
        });
    });
});

// POST: creates a new rides
router.post('/' + COLLECTION, function (req, res, next) {
    var newRide = req.body;

    if (!newRide.title) {
        handleError(res, "Invalid user input", "Must provide a title.", 400);
    } else {
        MongoClient.connect(URL, function(err, db) {
            if (err) {
                res.send(err);
            }
            db.collection(COLLECTION).insertOne(newRide, function(err, ride) {
                if (err) {
                    handleError(res, err.message, "Failed to create new ride.");
                } else {
                    res.status(201).json(ride.ops[0]);
                }
                db.close();
            });
        });
    }
});

// GET: find rides by id
router.get('/' + COLLECTION + '/:id', function (req, res, next) {
    MongoClient.connect(URL, function(err, db) {
        if (err) throw err;
        db.collection(COLLECTION).findOne({_id: new ObjectID(req.params.id)}, function(err, ride) {
            if (err) {
                handleError(res, err.message, "Failed to get ride");
            } else {
                res.status(200).json(ride);
            }
            db.close();
        });
    });
});

// PUT: update rides by id
router.put('/' + COLLECTION + '/:id', function (req, res, next) {
    var updateRide = req.body;

    if (updateRide._id) {
        delete updateRide._id;

        MongoClient.connect(URL, function(err, db) {
            if (err) throw err;
            db.collection(COLLECTION).update({_id: new ObjectID(req.params.id)}, updateRide, function(err, ride) {
                if (err) {
                    handleError(res, err.message, "Failed to update ride");
                } else {
                    updateRide._id = req.params.id;
                    res.status(200).json(updateRide);
                }
                db.close();
            });
        });
    } else {
        handleError(res, "Invalid update item", "Item without ID.", 400);
    }
});

// DELETE: deletes rides by id
router.delete('/' + COLLECTION + '/:id', function (req, res, next) {
    MongoClient.connect(URL, function(err, db) {
        if (err) throw err;
        db.collection(COLLECTION).remove({_id: new ObjectID(req.params.id)}, function(err, ride) {
            if (err) {
                handleError(res, err.message, "Failed to delete ride");
            } else {
                res.status(200).json(req.params.id);
            }
            db.close();
        });
    });

});

module.exports = router;
