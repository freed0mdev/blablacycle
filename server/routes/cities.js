var express = require('express');
var router = express.Router();
var mongoJs = require('mongojs');
var db = mongoJs('mongodb://yokodima:8063831qwer@ds145415.mlab.com:45415/umtb', ['cities']);

// Get All Cities
router.get('/cities', function (req, res, next) {
    db.cities.find(function (err, cities) {
        if (err) {
            res.send(err);
        }
        res.json(cities);
    });
});

module.exports = router;
