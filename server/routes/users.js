var express = require('express');
var router = express.Router();
var mongoJs = require('mongojs');
var db = mongoJs('mongodb://yokodima:8063831qwer@ds145415.mlab.com:45415/umtb', ['users']);

// Get All Tasks
router.get('/users', function (req, res, next) {
    db.users.find(function (err, users) {
        if (err) {
            res.send(err);
        }
        res.json(users);
    });
});

// Get Single Task
router.get('/user/:id', function (req, res, next) {
    db.users.findOne({_id: mongoJs.ObjectID(req.params.id)}, function (err, user) {
        if (err) {
            res.send(err);
        }
        res.json(user);
    });
});

// Save User
router.post('/user', function (req, res, next) {
    var user = req.body;
    if (!user.username) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.users.save(user, function (err, user) {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
});

// Delete User
router.delete('/user/:id', function (req, res, next) {
    db.users.remove({_id: mongoJs.ObjectID(req.params.id)}, function (err, user) {
        if (err) {
            res.send(err);
        }
        res.json(user);
    });
});

// Update User
router.put('/user/:id', function (req, res, next) {
    var user = req.body;
    var updUser = {};

    if (user.title) {
        updUser = user;
        delete updUser._id;
    }

    if (!updUser) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.users.update({_id: mongoJs.ObjectID(req.params.id)}, updUser, {},  function (err, user) {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
});

module.exports = router;
