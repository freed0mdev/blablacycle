import { BlablacyclePage } from './app.po';

describe('blablacycle App', () => {
  let page: BlablacyclePage;

  beforeEach(() => {
    page = new BlablacyclePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
