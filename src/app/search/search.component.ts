import {Component, OnInit, Input, Output} from '@angular/core';
import {Ride} from '../classes/ride';
import {City} from '../classes/city';
import {RideService} from '../services/ride.service';
import {CityService} from '../services/city.service';
import {IMyDateModel, IMyOptions} from 'mydatepicker';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
    @Input() rides: Ride[];
    @Output() cities: City[];
    ridesFiltered: Ride[];
    city: City;
    date: IMyDateModel;

    myDatePickerOptions: IMyOptions = {
        width: '202px',
        height: '43px',
        openSelectorOnInputClick: true,
        selectionTxtFontSize: '15px',
        dateFormat: 'dd-mm-yyyy',
        disableUntil: {
            year: new Date().getFullYear(),
            month: (new Date().getMonth() + 1),
            day: (new Date().getDay() - 1)
        }
    };

    constructor(private rideService: RideService, private cityService: CityService) {
        this.rideService.getRides().subscribe(rides => {
            this.rides = rides;
        });
        this.cityService.getCities().subscribe(cities => {
            this.cities = cities;
        });
    }

    ngOnInit() {
    }

    searchRides() {
        const city = this.city.description.toLowerCase();
        this.ridesFiltered = this.rides.filter(item => {
            return (item.city.description.toLowerCase() === city && item.date.epoc === this.date.epoc);
        });
    }

    onDateChanged(event: IMyDateModel) {
        this.date = event;
    }

}
