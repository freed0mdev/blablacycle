import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {RoutingModule} from "./routing/routing.module";
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './home/home.component';
import {SearchComponent} from './search/search.component';
import {RideComponent} from './ride/ride/ride.component';
import {RideListComponent} from './ride/ride-list/ride-list.component';
import {RideFilterComponent} from './ride/ride-filter/ride-filter.component';
import {RideEditComponent} from './ride/ride-edit/ride-edit.component';
import {RideCreateComponent} from './ride/ride-create/ride-create.component';
import {RideMapComponent} from './ride/ride-map/ride-map.component';
import {RideDetailsComponent} from './ride/ride-details/ride-details.component';
import {UserCreateComponent} from './user/user-create/user-create.component';
import {UserComponent} from './user/user/user.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {UserRidesComponent} from './user/user-rides/user-rides.component';
import {UserMessagesComponent} from './user/user-messages/user-messages.component';
import {AlertService} from "./services/alert.service";
import {AlertComponent} from './directives/alert/alert.component';
import {RegisterComponent} from './auth/register/register.component';
import {LoginComponent} from "./auth/login/login.component";
import {MyDatePickerModule} from "mydatepicker";
import {Ng2AutoCompleteModule} from "ng2-auto-complete";
import {UserService} from "./services/user.service";
import {AuthenticationService} from "./services/authentication.service";
import {CityService} from "./services/city.service";
import {RideService} from "./services/ride.service";
import {AppConfig} from "./app.config";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        SearchComponent,
        RideComponent,
        RideListComponent,
        RideFilterComponent,
        RideEditComponent,
        RideCreateComponent,
        RideMapComponent,
        RideDetailsComponent,
        UserCreateComponent,
        UserComponent,
        UserEditComponent,
        UserProfileComponent,
        UserRidesComponent,
        UserMessagesComponent,
        AlertComponent,
        LoginComponent,
        RegisterComponent
    ],
    imports: [
        RoutingModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        Ng2AutoCompleteModule,
        MyDatePickerModule
    ],
    providers: [AlertService, UserService, AuthenticationService, CityService, RideService, AppConfig],
    bootstrap: [AppComponent]
})
export class AppModule {
}
