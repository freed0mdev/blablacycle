import {Component, OnInit} from '@angular/core';
import {AlertService} from "../../services/alert.service";

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

    messages: any[];

    constructor(private alertService: AlertService) {
        this.messages = [];
    }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => {
            if (typeof message != 'undefined') {
                this.addMessage(message);
            }
        });
    }

    addMessage(message) {
        let messageItem = message;
        this.messages.push(messageItem);

        setTimeout(() => {
            this.removeMessage(messageItem);
        }, 10000);
    }

    removeMessage(message) {
        let index = this.messages.indexOf(message);
        this.messages.splice(index, 1);
    }

    close(event, message) {
        event.preventDefault();
        this.removeMessage(message);
    }

}
