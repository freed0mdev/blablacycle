import {Component, OnInit, Output} from '@angular/core';
import 'rxjs/add/operator/switchMap';
import {ActivatedRoute, Params} from "@angular/router";
import {City} from "../../classes/city";
import {Ride} from "../../classes/ride";
import {RideService} from "../../services/ride.service";
import {CityService} from "../../services/city.service";
import {AlertService} from "../../services/alert.service";
import {IMyOptions, IMyDateModel} from "mydatepicker";

@Component({
  selector: 'app-ride-edit',
  templateUrl: './ride-edit.component.html',
  styleUrls: ['./ride-edit.component.scss']
})

export class RideEditComponent implements OnInit {
  @Output() cities: City[];
  city: City;
  ride: Ride;

  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableUntil: {year: new Date().getFullYear(), month: (new Date().getMonth() + 1), day: (new Date().getDay() - 1)}
  };

  constructor(private rideService: RideService, private route: ActivatedRoute, private cityService: CityService, private alertService: AlertService) {
    this.cityService.getCities().subscribe(cities => {
      this.cities = cities;
    });
  }

  ngOnInit() {
    this.route.params
        .switchMap((params: Params) => this.rideService.getRide(params['id']))
        .subscribe(ride => this.ride = ride);
  }

  saveRide() {
    this.rideService.editRide(this.ride).subscribe(res => {this.alertService.success("Ride saved", true);});
  }

  onDateChanged(event: IMyDateModel) {
    this.ride.date = event;
  }
}
