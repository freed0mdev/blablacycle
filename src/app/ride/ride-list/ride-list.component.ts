import {Component, OnInit, Input} from '@angular/core';
import {Ride} from "../../classes/ride";
import {RideService} from "../../services/ride.service";
import {AlertService} from "../../services/alert.service";

@Component({
  selector: 'app-ride-list',
  templateUrl: './ride-list.component.html',
  styleUrls: ['./ride-list.component.scss']
})
export class RideListComponent implements OnInit {
  @Input() rides: Ride[];

  constructor(private rideService: RideService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.rideService.getRides().subscribe(rides => {
      this.rides = rides;
    });
  }

  remove(ride: Ride): void {
    this.rideService.deleteRide(ride).subscribe(res => {
      let index = this.rides.indexOf(ride);

      if (index > -1) {
        this.rides.splice(index, 1);
      }

      if (res.ok) {
        this.alertService.success(`Ride ${res['_body']} deleted`, true);
      } else {
        this.alertService.error('Removing failed', true);
      }
    });
  }
}
