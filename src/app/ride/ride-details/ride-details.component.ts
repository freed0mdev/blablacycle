import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {Ride} from "../../classes/ride";
import {RideService} from "../../services/ride.service";

@Component({
  selector: 'app-ride-details',
  templateUrl: './ride-details.component.html',
  styleUrls: ['./ride-details.component.scss']
})
export class RideDetailsComponent implements OnInit {
  @Input() ride: Ride;

  constructor(private rideService: RideService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params
        .switchMap((params: Params) => this.rideService.getRide(params['id']))
        .subscribe(ride => this.ride = ride);
  }

}
