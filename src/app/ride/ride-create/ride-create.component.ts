import {Component, OnInit, Output} from '@angular/core';
import {City} from "../../classes/city";
import {RideService} from "../../services/ride.service";
import {CityService} from "../../services/city.service";
import {AlertService} from "../../services/alert.service";
import {Ride} from "../../classes/ride";
import {IMyDateModel, IMyOptions} from "mydatepicker";

@Component({
  selector: 'app-ride-create',
  templateUrl: './ride-create.component.html',
  styleUrls: ['./ride-create.component.scss']
})
export class RideCreateComponent implements OnInit {
  title: string = '';
  date: IMyDateModel;
  distance: number = null;
  author: string = '';
  @Output() cities: City[];
  city: City;
  region: string = '';
  dateFormatted: string;

  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd-mm-yyyy',
    width: '234px',
    disableUntil: {year: new Date().getFullYear(), month: (new Date().getMonth() + 1), day: (new Date().getDay() - 1)}
  };

  constructor(private rideService: RideService, private cityService: CityService, private alertService: AlertService) {
    this.cityService.getCities().subscribe(cities => {
      this.cities = cities;
    });
  }

  ngOnInit() {
  }

  createRide() {
    let ride = new Ride(this.title, this.date, this.distance, this.author, this.city, this.region);
    this.rideService.createRide(ride).subscribe(ride => {
      this.title = '';
      this.dateFormatted = '';
      this.distance = null;
      this.author = '';
      this.city = null;
      this.region = '';
    });
    this.alertService.success("Ride created", true);
  }

  onDateChanged(event) {
    this.date = event;
    this.dateFormatted = event.formatted;
  }
}
