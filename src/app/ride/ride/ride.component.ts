import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Ride} from "../../classes/ride";

@Component({
    selector: 'app-ride',
    templateUrl: './ride.component.html',
    styleUrls: ['./ride.component.scss']
})
export class RideComponent implements OnInit {
    @Input() ride: Ride;
    @Output() remove: EventEmitter<Ride> = new EventEmitter<Ride>();

    constructor() {
    }

    ngOnInit() {
    }

    onRemove() {
        this.remove.emit(this.ride);
    }
}
