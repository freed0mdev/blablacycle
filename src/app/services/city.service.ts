import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {City} from "../classes/city";

@Injectable()
export class CityService {
    private apiUrl = 'api/cities';
    // private apiKey = 'AIzaSyBntieOgGzn8hh9w2T_iBE4yyqM16ADNeM';
    // private apiUrlGoogle = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Кие&types=geocode&language=ru&key=${this.apiKey}`;

    constructor(private http: Http) {
    }

    getCities(): Observable<City[]> {
        return this.http.get(this.apiUrl)
            .map(res => res.json() as City[])
            .catch(this.handleError);
    }

    private handleError(err: any) {
        console.error('getCities', err);
        return Observable.throw(err.message || err)
    }
}
