import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {Ride} from "../classes/ride";

@Injectable()
export class RideService {
    private apiUrl = 'api/rides';

    constructor(private http: Http) {
    }

    getRides(): Observable<Ride[]> {
        return this.http.get(this.apiUrl)
            .map(res => res.json() as Ride[])
            .catch(this.handleError);
    }

    getRide(id: number): Observable<Ride> {
        return this.http.get(this.apiUrl + '/' + id)
            .map(res => res.json() as Ride[])
            .catch(this.handleError);
    }

    createRide(ride: Ride): Observable<Ride> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});

        return this.http.post(this.apiUrl, ride, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    deleteRide(ride: Ride) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        let url = `${this.apiUrl}/${ride['_id']}`;

        return this.http.delete(url, options)
            .catch(this.handleError);
    }

    editRide(ride: Ride) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        let url = `${this.apiUrl}/${ride['_id']}`;

        return this.http.put(url, ride, options)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        let errMsg = (err.message) ? err.message :
            err.status ? `${err.status} - ${err.statusText}` : 'Server error';
        console.error('getRides', err);

        return Observable.throw(errMsg || err);
    }

}
