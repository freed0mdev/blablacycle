import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from "@angular/router";
import {RideListComponent} from "../ride/ride-list/ride-list.component";
import {RideCreateComponent} from "../ride/ride-create/ride-create.component";
import {UserProfileComponent} from "../user/user-profile/user-profile.component";
import {UserCreateComponent} from "../user/user-create/user-create.component";
import {UserEditComponent} from "../user/user-edit/user-edit.component";
import {UserRidesComponent} from "../user/user-rides/user-rides.component";
import {UserMessagesComponent} from "../user/user-messages/user-messages.component";
import {HomeComponent} from "../home/home.component";
import {RideEditComponent} from "../ride/ride-edit/ride-edit.component";
import {RideDetailsComponent} from "../ride/ride-details/ride-details.component";

const ROUTES: Routes = [
    {
        path: 'rides',
        component: RideListComponent,
        data: { title: 'Rides' },
        children: [
            {path: 'details/:id', component: RideDetailsComponent, data: { title: 'Ride Details' }},
            {path: 'edit/:id', component: RideEditComponent, data: { title: 'Ride Edit' }},
            {path: 'create', component: RideCreateComponent, data: { title: 'Ride Create' }},
        ]
    },
    {
        path: 'user-profile',
        component: UserProfileComponent,
        data: { title: 'Profile' }
    },
    {
        path: 'user-create',
        component: UserCreateComponent,
        data: { title: 'Create User' }
    },
    {
        path: 'user-edit',
        component: UserEditComponent,
        data: { title: 'Edit User' }
    },
    {
        path: 'user-rides',
        component: UserRidesComponent,
        data: { title: 'User Rides' }
    },
    {
        path: 'user-messages',
        component: UserMessagesComponent,
        data: { title: 'User Messages' }
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(ROUTES)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class RoutingModule {
}
