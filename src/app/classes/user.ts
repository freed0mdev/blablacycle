export class User {
    _id: string;
    constructor(
        public username: string,
        public password: string,
        public firstName: string,
        public lastName: string
    ) {}
}