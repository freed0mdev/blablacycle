import {IMyDateModel} from "mydatepicker";
import {City} from "./city";

export class Ride {
    _id?: string;

    constructor(public title: string,
                public date: IMyDateModel,
                public distance: number,
                public author: string,
                public city: City,
                public region: string) {
    }
}
